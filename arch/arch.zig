//! Stub file to make directory be recognized as package
pub const x86 = struct {
    pub const cpu = @import("x86/cpu.zig");
    pub const fpu = @import("x86/fpu.zig");
    pub const gdt = @import("x86/gdt.zig");
    pub const idt = @import("x86/idt.zig");
    pub const isr = @import("x86/isr.zig");
    pub const pic = @import("x86/pic.zig");
    pub const layout = @import("x86/layout.zig");
    pub const selector = @import("x86/selector.zig");
};
