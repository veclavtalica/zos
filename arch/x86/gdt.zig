const std = @import("std");
const cpu = @import("cpu.zig");
const layout = @import("layout.zig");
const Selector = @import("selector.zig").Selector;

// https://wiki.osdev.org/Global_Descriptor_Table

pub const Descriptor = packed struct {
    limit_low: u16,
    base_low: u24,
    access: Access,
    limit_high: u4,
    flags: Flags,
    base_high: u8,

    // TODO: Check that base + limit will not exceed 4GiB on page granularity set
    pub fn init(base: u32, limit: u20, access: Access, flags: Flags) Descriptor {
        return Descriptor{
            .limit_low = @truncate(u16, limit),
            .base_low = @truncate(u24, base),
            .access = access,
            .limit_high = @truncate(u4, limit >> 16),
            .flags = flags,
            .base_high = @truncate(u8, base >> 24),
        };
    }
};

pub const Register = packed struct {
    limit: u16,
    base: *const Descriptor,
};

var gdtr align(4) = Register{
    .limit = undefined,
    .base = undefined,
};

// TODO: Simplify, possibly with unions
pub const Access = packed struct {
    // TODO: task_state_segment
    _accessed: u1 = 0,
    read_or_write: u1,
    strict_or_grows_down: u1,
    code_segment: u1,
    not_task_state_system_segment: u1,
    privilege: Selector.Privilege,
    present: Present,

    pub const Present = enum(u1) { not_present, present };

    // Code segment specific:
    pub const Readable = enum(u1) { not_readable, readable };
    /// Defines whether code segment might only be executed from the defined privilege
    pub const Strictness = enum(u1) { not_strict, strict };

    // Data segment specific:
    pub const Writable = enum(u1) { not_writable, writable };
    pub const GrowingDir = enum(u1) { grows_up, grows_down };

    pub fn init_code_segment(readable: Readable, strictness: Strictness, privilege: Selector.Privilege, present: Present) Access {
        return .{
            .read_or_write = @enumToInt(readable),
            .strict_or_grows_down = @enumToInt(strictness),
            .code_segment = 1,
            .not_task_state_system_segment = 1,
            .privilege = privilege,
            .present = present,
        };
    }

    pub fn init_data_segment(writable: Writable, growing_dir: GrowingDir, privilege: Selector.Privilege, present: Present) Access {
        return .{
            .read_or_write = @enumToInt(writable),
            .strict_or_grows_down = @enumToInt(growing_dir),
            .code_segment = 0,
            .not_task_state_system_segment = 1,
            .privilege = privilege,
            .present = present,
        };
    }
};

pub const Flags = packed struct {
    _reserved: u2 = 0,
    is_protected_mode: u1,
    granuality: Granuality,

    pub const Granuality = enum(u1) {
        byte,
        page,
    };

    pub const Mode = enum {
        real,
        protected,
    };

    pub fn init(m: Mode, g: Granuality) Flags {
        return switch (m) {
            .real => Flags{ .is_protected_mode = 0, .granuality = g },
            .protected => Flags{ .is_protected_mode = 1, .granuality = g },
        };
    }
};

pub fn load(gdt: []const Descriptor) void {
    std.debug.assert(gdt.len < 256);
    // std.debug.assert(std.mem.eql(Descriptor, idt[0..0], std.mem.zeroes(Descriptor)));
    gdtr.base = &gdt[0];
    gdtr.limit = @intCast(u16, @sizeOf(Descriptor) * gdt.len - 1);
    cpu.loadGdt(&gdtr);
}

comptime {
    const assert = @import("std").debug.assert;
    assert(@bitSizeOf(Descriptor) == 64);
    assert(@bitSizeOf(Register) == 48);
    assert(@bitSizeOf(Access) == 8);
    assert(@bitSizeOf(Flags) == 4);
}
