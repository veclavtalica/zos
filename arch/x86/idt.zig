const std = @import("std");
const cpu = @import("cpu.zig");
const layout = @import("layout.zig");
const Selector = @import("selector.zig").Selector;

// https://wiki.osdev.org/Interrupt_Descriptor_Table

// TODO: Using callconv(.Interrupt) defined functions for user and kernel defined interrupts

pub const Descriptor = packed struct {
    offset_low: u16,
    selector: Selector,
    _reserved: u8 = 0,
    type: Type,
    _unused: u1 = 0,
    privilege: Selector.Privilege,
    _present: u1 = 1,
    offset_high: u16,

    pub fn init(
        idt_type: Descriptor.Type,
        selector: Selector,
        /// Note that its different from sector privilege, it denotes privilege needed to be able to call the interrupt
        privilege: Selector.Privilege,
        f: fn () callconv(.Naked) void,
    ) Descriptor {
        return .{
            .offset_low = @truncate(u16, @ptrToInt(f)),
            .selector = selector,
            .type = idt_type,
            .privilege = privilege,
            .offset_high = @truncate(u16, @ptrToInt(f) >> 16),
        };
    }

    pub const Type = enum(u4) {
        task_gate = 0x5,
        interrupt_gate16 = 0x6,
        trap_gate16 = 0x7,
        interrupt_gate32 = 0xe,
        trap_gate32 = 0xf,
    };
};

pub const Register = packed struct {
    limit: u16,
    base: *const Descriptor,
};

var idtr align(4) = Register{
    .limit = undefined,
    .base = undefined,
};

pub fn load(idt: []const Descriptor) void {
    std.debug.assert(idt.len < 256);
    idtr.base = &idt[0];
    idtr.limit = @intCast(u16, @sizeOf(Descriptor) * idt.len - 1);
    cpu.loadIdt(&idtr);
}

comptime {
    const assert = std.debug.assert;
    assert(@bitSizeOf(Descriptor) == 64);
    assert(@bitSizeOf(Register) == 48);
}
