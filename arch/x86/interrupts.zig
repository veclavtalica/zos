// https://wiki.osdev.org/Interrupt_Service_Routines
// https://www.felixcloutier.com/x86/intn:into:int3:int1

// TODO: We might need to switch to kernel space stack as soon as possible, with setting up SS register appropriately
//       Currently we're pusing interrupt context directly to the stack from which interrupt happened, which might be problematic

const std = @import("std");

fn generateIsr(comptime int: u8) fn () callconv(.Naked) void {
    return struct {
        fn func() align(4) callconv(.Naked) void {
            const prototype =
                \\ cli
                \\ .if (({0} != 8) && !(({0} >= 10) && ({0} <= 14)) && ({0} != 17))
                \\   push $0            // Polyfill error number for cases where none is supplied
                \\ .endif
                \\ push ${0}            // Push the interrupt number
                \\ jmp commonIsrRoutine // Jump to the common handler
            ;
            const code = comptime std.fmt.comptimePrint(prototype, .{int});
            asm volatile (code);
        }
    }.func;
}

fn generateIrq(comptime irq: u8, comptime int: u8) fn () callconv(.Naked) void {
    return struct {
        fn func() align(4) callconv(.Naked) void {
            const prototype =
                \\ cli
                \\ push ${0}            // Push IRQ identification (line its triggered by)
                \\ push ${1}            // Push the interrupt number
                \\ jmp commonIsrRoutine // Jump to the common handler
            ;
            const code = comptime std.fmt.comptimePrint(prototype, .{irq, int});
            asm volatile (code);
        }
    }.func;
}

export fn commonIsrRoutine() align(4) callconv(.Naked) noreturn {
    const layout = @import("layout.zig");
    const prototype =
        \\ pusha        // Save the registers state
        \\ pushl %%ds    // Save data segment selector
        \\ // TODO: Note that not all registers are pushed with pusha, notably EBX, we might need to preserve more state information
        \\ // TODO: Check whether interrupt is already from kernel, as it will break the stack
        \\ cld
        \\
        \\ // TODO: Save previous data segment values for future resetting, there's no guarantee that on exit user space assumption is valid
        \\ // Setup kernel data segment.
        \\ mov {[kernel_data_segment_idx]}, %%ax
        \\ mov %%ax, %%ds
        \\ mov %%ax, %%fs
        \\ mov %%ax, %%es
        \\ mov %%ax, %%gs
        \\
        \\ // Save the pointer to the current context and switch to the kernel stack
        \\ // TODO: Shouldn't it be offset to point to base of structure, instead of end?
        \\ mov %%esp, current_context
        \\ mov $__kernel_stack_head, %%esp
        \\
        \\ call interruptDispatch  // Handle the interrupt event.
        \\
        \\ // Restore the pointer to the context (of a different thread, potentially)
        \\ mov current_context, %%esp
        \\
        \\ // Restore previous data segment
        \\ pop %%eax
        \\ mov %%ax, %%ds
        \\ mov %%ax, %%fs
        \\ mov %%ax, %%es
        \\ mov %%ax, %%gs
        \\
        \\ popa          // Restore the registers state
        \\ add $8, %%esp // Remove interrupt number and error code from stack
        \\ iret
    ;
    const code = comptime std.fmt.comptimePrint(prototype, .{
        .kernel_data_segment_idx = layout.kernel_data_segment_idx,
    });
    asm volatile (code);
    unreachable;
}

comptime {
    var i: comptime_int = 0;
    inline while (i < 32) : (i += 1) {
        const isr = generateIsr(i);
        const options = std.builtin.ExportOptions{ .name = std.fmt.comptimePrint("isr{any}", .{i}) };
        @export(isr, options);
    }
}

comptime {
    var i: comptime_int = 0;
    inline while (i < 16) : (i += 1) {
        const irq = generateIrq(i, i + 32);
        const options = std.builtin.ExportOptions{ .name = std.fmt.comptimePrint("isr{any}", .{i + 32}) };
        @export(irq, options);
    }
}
