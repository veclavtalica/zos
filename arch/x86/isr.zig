const std = @import("std");
const cpu = @import("cpu.zig");
const idt = @import("idt.zig");
const pic = @import("pic.zig");
const layout = @import("layout.zig");

// https://wiki.osdev.org/Interrupt_Service_Routines

// TODO: Check whether its well formed from ISRs
// TODO: IRQs have different field semantics, we might need to define union for that
// TODO: Field to denote whether current context comes from user or kernel space
const Context = packed struct {
    ds: u32,
    registers: cpu.RegisterPack,
    interrupt: u32,
    error_code: u32,
    eip: u32,
    cs: u32,
    eflags: u32,
    esp: u32,
    ss: u32,
};

// TODO: Infer contexts from stack, this will allow nesting interrupts in kernel space
//       Or at least have a stack of context frame pointers
export var current_context: *volatile Context = undefined;

// TODO: Should be growable for user and kernel space defined interrupts
var interrupt_handlers = [_]fn () void{defaultHandler} ** 48;

export fn interruptDispatch() void {
    const int = @intCast(u8, current_context.interrupt);
    switch (int) {
        // Exceptions
        0...31 => {
            interrupt_handlers[int]();
        },
        // IRQs triggered by PIC
        32...47 => {
            // TODO: Handle spurious
            interrupt_handlers[int]();
            if (int >= 40)
                cpu.writePort(pic.pic1_command_port, pic.end_of_interrupt);
            cpu.writePort(pic.pic0_command_port, pic.end_of_interrupt);
        },
        // Anything kernel defined
        else => unreachable,
    }
}

fn defaultHandler() noreturn {
    const int = current_context.interrupt;
    // TODO: Mechanism to easily define stack buffer prints with maximum known length
    var buf: [32]u8 = undefined;
    @panic(std.fmt.bufPrint(buf[0..], "unhandled interrupt: {d}", .{int}) catch unreachable);
}

/// Initialize interrupt vector with default handlers, it includes both Intel exceptions and PIC IRQs mapped starting from index 32
pub fn setDefaults(idt_gates: *[48]idt.Descriptor) void {
    comptime var i: u8 = 0;
    inline while (i < 48) : (i += 1) {
        const options = comptime std.builtin.ExternOptions{ .name = std.fmt.comptimePrint("isr{d}", .{i}) };
        const isr = @extern(fn () callconv(.Naked) void, options);
        idt_gates[i] = idt.Descriptor.init(.interrupt_gate32, layout.kernel_code_selector, .user, isr);
    }
}
