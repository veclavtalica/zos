const std = @import("std");

// https://wiki.osdev.org/Segment_selector

pub const Selector = packed struct {
    privilege: Privilege,
    table: Table,
    idx: u13,

    pub const Privilege = enum(u2) {
        kernel = 0b00,
        user = 0b11,
    };

    pub const Table = enum(u1) {
        gdt,
        ldt,
    };
};

comptime {
    const assert = std.debug.assert;
    assert(@bitSizeOf(Selector) == 16);
}
