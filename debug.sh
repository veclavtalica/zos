emu=qemu
img='./zos.img'

# http://www.neuraldk.org/document.php?grubKernel

if [ -n $1 ]; then
  emu=$1
fi

if [ ${emu} == 'qemu' ]; then
  qemu-system-i386 -s zig-out/bin/os -d int & \
    gdb \
      -ex 'target remote localhost:1234' \
      -ex 'symbol-file zig-out/bin/os' \
      -ex 'maintenance packet Qqemu.sstep=0x5' \
      -ex 'b kernelMain' -ex 'c'
elif [ ${emu} == "bochs" ]; then
  # bochs expected devices to be sector (512 byte) sized
  size=$(wc -c < ${img})
  size=$(((${size} / 512) * 512))
  truncate -s ${size} ./zos.img
  bochs 'boot:disk' 'ata0-master: type=disk, path="'${img}'", mode=flat'
else
  echo unknown emulator: ${emu}
fi
