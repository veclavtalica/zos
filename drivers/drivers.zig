//! Stub file to make directory be recognized as package
pub const vga = struct {
    pub const printer = @import("vga/printer.zig");
};

pub const printer = @import("printer.zig");
