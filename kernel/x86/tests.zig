//! Test runner to check assumption kernel makes about the system
const std = @import("std");
const Writer = @import("drivers").printer.Writer;

const Error = error{Failure};

// TODO: Test VGA
// TODO: Test exception interrupts
// TODO: Test IRQ interrupts (via timer)

pub fn run(writer: *Writer) void {
    wrap("fpu", writer);
}

fn expect(s: bool) !void {
    if (!s) return Error.Failure;
}

inline fn wrap(comptime proc_sym: []const u8, writer: *Writer) void {
    writer.context.stateSetColor(.white, .black);
    try writer.writeAll(comptime std.fmt.comptimePrint("testing {s}: ", .{proc_sym}));
    if (@call(.{}, @field(@This(), proc_sym), .{})) {
        writer.context.stateSetColor(.green, .black);
        try writer.writeAll("ok\n");
    } else |_| {
        writer.context.stateSetColor(.red, .black);
        try writer.writeAll("fail\n");
    }
}

fn fpu() !void {
    @setFloatMode(.Strict);
    {
        // TODO: Make sure optimizer will not make it invalid
        const Punner = packed union {
            i32: i32,
            f32: f32,
        };
        // TODO: Move machine epsilon calculation to shared code
        // TODO: Afaik number we're using here will impact resulting epsilon, we might need to pick the biggest
        const root = @as(f32, 512512.0);
        var punner = Punner{ .f32 = root };
        punner.i32 += 1;
        const epsilon = punner.f32 - root;

        const a: f32 = 100.0;
        const b: f32 = 200.0;
        const t = a + b;
        try expect(std.math.fabs(t - 300.0) < epsilon);
    }
}
